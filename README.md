# 0.vbs

![](https://img.shields.io/badge/written%20in-Bash%2C%20VBScript-blue)

A script that renames itself when run, as a counter

Double-click the script, and it becomes the next number. Intended to keep track of which TV episode you are up to in a folder.

Zip file version is included to avoid restricted HTTP proxies.

## Changelog

2017-10-04 tar
- Port to bash
- [⬇️ 0.tar](dist-archive/0.tar) *(10.00 KiB)*


2008-06-12 vbs
- Initial release (VBScript)
- [⬇️ 0.vbs.zip](dist-archive/0.vbs.zip) *(280B)*
- [⬇️ 0.vbs](dist-archive/0.vbs) *(174B)*

